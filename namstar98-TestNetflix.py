#!/usr/bin/env python3

# -------
# imports
# -------
from Netflix import netflix_eval, prediction_equation
from unittest import main, TestCase
from math import sqrt
from io import StringIO
from numpy import sqrt, square, mean, subtract

# -----------
# TestNetflix
# -----------

class TestNetflix (TestCase):

    # ----
    # eval
    # ----

    def test_eval_1(self):
        r = StringIO("10040:\n2417853\n1207062\n2487973\n")
        w = StringIO()
        netflix_eval(r, w)
        self.assertEqual(
            w.getvalue(), "10040:\n3.486\n3.446\n3.95\n0.88\n")
    def test_eval_2(self):
        a = StringIO("50:\n2544332\n2614700\n2188684\n1916105\n454329\n")
        b = StringIO()
        netflix_eval(a, b)
        self.assertEqual(
            b.getvalue(), "50:\n4.177\n3.49\n3.45\n3.894\n3.259\n0.47\n")

    def test_eval_3(self):
        j = StringIO("5002:\n2231079\n230673\n2107133\n")
        k = StringIO()
        netflix_eval(j, k)
        self.assertEqual(
            k.getvalue(), "5002:\n3.205\n3.08\n3.597\n1.02\n")

    def test_prediction_1(self):
        self.assertEqual(prediction_equation(1515111, 10003), 3.043)
    def test_prediction_2(self):
        self.assertEqual(prediction_equation(1473765, 9999), 2.682)
    def test_prediction_3(self):
        self.assertEqual(prediction_equation(14756, 1), 3.682)
    def test_prediction_4(self):
        self.assertEqual(prediction_equation(1972287, 14062), 3.827)

# ----
# main
# ----			
if __name__ == '__main__':
    main()

""" #pragma: no cover
% coverage3 run --branch TestNetflix.py >  TestNetflix.out 2>&1



% coverage3 report -m                   >> TestNetflix.out



% cat TestNetflix.out
.
----------------------------------------------------------------------
Ran 1 test in 0.000s

OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Netflix.py          27      0      4      0   100%
TestNetflix.py      13      0      0      0   100%
------------------------------------------------------------
TOTAL               40      0      4      0   100%

"""
