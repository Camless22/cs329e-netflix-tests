#!/usr/bin/env python3

# -------
# imports
# -------
from Netflix import netflix_eval
from unittest import main, TestCase
from math import sqrt
from io import StringIO
from numpy import sqrt, square, mean, subtract

# -----------
# TestNetflix
# -----------

class TestNetflix (TestCase):

    # ----
    # eval
    # ----

    def test_eval_1(self):

        r = StringIO("1:\n30878\n2647871\n1283744\n2488120\n")
        w = StringIO()
        netflix_eval(r, w)
        self.assertEqual(
            w.getvalue(), "1:\n3.66\n3.52\n3.61\n4.20\n0.58\n")

    def test_eval_2(self):
        r = StringIO("10:\n1952305\n1531863\n1000:\n2326571\n977808\n")
        w = StringIO()
        netflix_eval(r,w)
        self.assertEqual(
            w.getvalue(), "10:\n3.29\n3.16\n1000:\n3.43\n3.25\n0.30\n")
    
    def test_eval_3(self):
        r = StringIO("10004:\n1737087\n1270334\n1262711\n1903515\n")
        w = StringIO()
        netflix_eval(r,w)
        self.assertEqual(
            w.getvalue(), "10004:\n4.37\n3.88\n4.0\n3.8\n1.00\n")
    # Test for only one movie
    def test_eval_4(self):
        r = StringIO("9999:\n1473765\n")
        w = StringIO()
        netflix_eval(r,w)
        self.assertEqual(
            w.getvalue(), "9999:\n2.43\n0.43\n")
# ----
# main
# ----			
if __name__ == '__main__':
    main()

""" #pragma: no cover
% coverage3 run --branch TestNetflix.py >  TestNetflix.out 2>&1



% coverage3 report -m                   >> TestNetflix.out



% cat TestNetflix.out
.
----------------------------------------------------------------------
Ran 1 test in 0.000s

OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Netflix.py          27      0      4      0   100%
TestNetflix.py      13      0      0      0   100%
------------------------------------------------------------
TOTAL               40      0      4      0   100%

"""
