#!/usr/bin/env python3

# -------
# imports
# -------
from Netflix import netflix_eval
from unittest import main, TestCase
from math import sqrt
from io import StringIO
from numpy import sqrt, square, mean, subtract

# -----------
# TestNetflix
# -----------

class TestNetflix (TestCase):

    # ----
    # eval
    # ----

    #Testing the correct parsing of all values in netflix_eval - does output generate movie followed by ratings and rmse?
    def test_netflix_eval_parse(self):
        r = StringIO("1:\n30878\n2647871\n1283744\n")
        w = StringIO()
        netflix_eval(r, w)
        self.assertEqual(
            w.getvalue(), "1:\n3.54\n3.19\n3.46\n0.59\n")

    #Testing the correct parsing of movie_id - does movie_id remain equal to input during output?
    def test__movie_id_parse(self):
        r = StringIO("1:\n30878\n2647871\n1283744\n")
        w = StringIO()
        netflix_eval(r, w)
        list = w.getvalue().split()
        for element in list:
            element = element.strip()
        self.assertEqual(list[0],'1:')

    #Test the correct parsing of customer id - does customer_id become prediction in output?
    #value is going to change, LOOK OVER THIS ****
    def test_customer_id_parse(self):
        r = StringIO("1:\n30878\n2647871\n1283744\n")
        w = StringIO()
        netflix_eval(r, w)
        list = w.getvalue().split()
        for element in list:
            element = element.strip()
        self.assertEqual(list[1],'3.54')

    #Test the RMSE value is lower than 1.00 - does the last value equal an rmse value?
    def test_netflix_eval_rmse(self):
        r = StringIO("1:\n30878\n2647871\n1283744\n")
        w = StringIO()
        netflix_eval(r, w)
        list = w.getvalue().split()
        for element in list:
            element = element.strip()
        #assert less than 1??? ******
        self.assertEqual(list[-1],'0.59')

# ----
# main
# ----
if __name__ == '__main__':
    main()

""" #pragma: no cover
% coverage3 run --branch TestNetflix.py >  TestNetflix.out 2>&1



% coverage3 report -m                   >> TestNetflix.out



% cat TestNetflix.out
.
----------------------------------------------------------------------
Ran 1 test in 0.000s

OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Netflix.py          27      0      4      0   100%
TestNetflix.py      13      0      0      0   100%
------------------------------------------------------------
TOTAL               40      0      4      0   100%

"""
